module.exports = [{
    name: "Anakin Skywalker",
    phone: "+320557942467",
    photo: "assets/star-wars-images/Anakin-Skywalker_d3330724.jpeg"
  }, {
    name: "Han Solo",
    phone: "+320557826549",
    photo: "assets/star-wars-images/Han-Solo_1d08eb2e.jpeg"
  }, {
    name: "Obi-Wan Kenobi",
    phone: "+320554899876",
    photo: "assets/star-wars-images/Obi-Wan-Kenobi_6d775533.jpeg"
  }, {
    name: "Luke Skywalker",
    phone: "+320557984651",
    photo: "assets/star-wars-images/Luke-Skywalker_dd9c9f9b.jpeg"
  }, {
    name: "Leia Organa",
    phone: "+320557951352",
    photo: "assets/star-wars-images/Princess-Leia-Organa_d7761ff5.jpeg"
  }, {
    name: "Padme Amidala",
    phone: "+320558664243",
    photo: "assets/star-wars-images/Padme-Amidala_05d50c8a.jpeg"
  }, {
    name: "Yoda",
    phone: "+320553546987",
    photo: "assets/star-wars-images/Yoda-Retina_2a7ecc26.jpeg"
  }, {
    name: "Jar Jar Binks",
    phone: "+320557895435",
    photo: "assets/star-wars-images/databank_jarjarbinks_01_169_c70767ab.jpeg"
  }, {
    name: "Jango Fett",
    phone: "+320554307076",
    photo: "assets/star-wars-images/databank_jangofett_01_169_884cefab.jpeg"
  }, {
    name: "Darth Vader",
    phone: "+320554980656",
    photo: "assets/star-wars-images/Darth-Vader_6bda9114.jpeg"
  }, {
    name: "Emperor Palpatine",
    phone: "+320551354810",
    photo: "assets/star-wars-images/Emperor-Palpatine_7ac4a10e.jpeg"
  }, {
    name: "Count Dooku",
    phone: "+320557498462",
    photo: "assets/star-wars-images/Count-Dooku_4f552149.jpeg"
  }
];