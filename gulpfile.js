var del = require('del');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var combiner = require('stream-combiner2');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

var paths = new (function() {
  this.src = 'src';
  this.dest = 'public';
  this.styles = {
    src: this.src + '/styles/*.styl',
    watch: this.src + '/styles/**/*.styl',
    dest: this.dest + '/css',
    vendor: []
  };
  this.jade = {
    watch: 'views/**/*.jade',
  };
  this.js = {
    src: this.src + '/js/*.js',
    watch: this.src + '/js/*.js',
    dest: this.dest + '/js',
    vendor: [
      'node_modules/jquery/dist/jquery.min.js'
    ]
  };
  this.assets = {
    src: this.src + '/assets/**/*.*',
    watch: this.src + '/assets/**/*.*',
    dest: this.dest + '/assets'
  };
})();

gulp.task('styles', function() {
  combiner(
    gulp.src(paths.styles.src),
    $.if(isDevelopment, $.sourcemaps.init()),
    $.stylus(),
    $.autoprefixer(),
    $.if(isDevelopment, $.concat('style.css'), $.concat('style.min.css')),
    $.if(isDevelopment, $.sourcemaps.write(), $.cssnano()),
    gulp.dest(paths.styles.dest),
    reload({stream: true})
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'Styles',
      message: err.message
    };
  }));
});

gulp.task('styles:vendor', function() {
  combiner(
    gulp.src(paths.styles.vendor),
    $.concat('vendor.min.css'),
    $.cssnano(),
    gulp.dest(paths.styles.dest)
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'Styles Vendor',
      message: err.message
    };
  }));
});

gulp.task('js', function() {
  combiner(
    gulp.src(paths.js.src),
    $.if(isDevelopment, $.concat('all.js'), $.concat('all.min.js')),
    $.if(!isDevelopment, $.uglify()),
    gulp.dest(paths.js.dest),
    reload({stream: true})
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'JavaScript',
      message: err.message
    };
  }));
});

gulp.task('js:vendor', function() {
  combiner(
    gulp.src(paths.js.vendor),
    $.concat('vendor.min.js'),
    $.uglify(),
    gulp.dest(paths.js.dest)
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'JS Vendor',
      message: err.message
    };
  }));
});

gulp.task('serve', function() {
  browserSync.init({
    proxy: 'localhost:3000',
    notify: false
  });
});

gulp.task('assets', function() {
  gulp.src(paths.assets.src)
  .pipe($.newer(paths.assets.dest))
  .pipe(gulp.dest(paths.assets.dest));
});

gulp.task('watch', function() {
  gulp.watch(paths.js.watch, ['js']);
  gulp.watch(paths.styles.watch, ['styles']);
  gulp.watch(paths.assets.watch, ['assets']);
  browserSync.watch(paths.jade.watch).on('change', reload);
});

gulp.task('clean', function() {
  del(paths.dest);
});

gulp.task('build', [
  'styles',
  'js',
  'styles:vendor',
  'js:vendor',
  'assets'
]);

gulp.task('default', [
  'watch',
  'serve'
]);