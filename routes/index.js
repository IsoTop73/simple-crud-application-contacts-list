var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var ObjectId = mongodb.ObjectId;
var MongoClient = mongodb.MongoClient;


router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/contacts', function(req, res, next) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      res.status(500).send('Can\'t connect to Database');
      return;
    }

    db.collection('contacts').find().toArray(function(err, contacts) {
      if (!contacts.length) {
        res.status(500).send('Contacts not found');
        return;
      }
      
      res.send(contacts);
      db.close();
    });
  });
});

router.post('/add-contact', function(req, res, next) {
  var name = req.body.name;
  var phone = req.body.phone;

  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      return res.render('error', {
        message: 'Can\'t connect to Database',
        error: err
      });
    }

    var exist = db.collection('contacts').find({
      name: name,
      phone: phone
    }).count(function(err, count) {
      if (count >= 1) {
        res.status(400).send('This contact is already exist');
      } else {
        db.collection('contacts').insert({
          name: name,
          phone: phone
        }, function(err, result) {
          if (err) {
            res.status(500).send('Can\'t add the contact, try later');
            return;
          }

          if (result.result.ok) {
            res.send('Contact successfully added');
          }

          db.close();
        });
      }
    });
  });
});

router.delete('/delete-contacts', function(req, res, next) {
  var deleteIds = [].concat(req.body.deleteIds);

  deleteIds = deleteIds.map(function(elem) {
    return new ObjectId(elem);
  });

  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      return res.render('error', {
        message: 'Can\'t connect to Database',
        error: err
      });
    }

    db.collection('contacts').deleteMany({
      _id: {$in: deleteIds}
    },
    function(err, result) {
      var deletedCount = result.deletedCount;
      var successMessage = 'Successfully delete ' + deletedCount +
      (deletedCount < 2 ? ' contact' : ' contacts');
      var errorMessage = 'Can\'t dalete the contact, try later';

      if (err) {
        res.status(500).send(errorMessage);
        return;
      }

      if (result.result.ok) {
        res.status(200).send(successMessage);
      }

      db.close();
    });
  });
});

router.post('/update-contact', function(req, res, next) {
  var id = req.body.id;
  var newName = req.body.newName;
  var newPhone = req.body.newPhone;

  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      return res.render('error', {
        message: 'Can\'t connect to Database',
        error: err
      });
    }

    db.collection('contacts').updateOne({
      _id: new ObjectId(id)
    }, {
      $set: {
        name: newName,
        phone: newPhone
      }
    }, function(err, result) {
      if (err) {
        res.status(500).send('Can\'t update the contact, try later');
        return;
      }

      if (result.result.ok) {
        res.send('Contact successfully updated');
      }

      db.close();
    });
  });
});

router.get('/reset-db', function(req, res, next) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      res.status(500).send('Can\'t reset DB, try later');
      return;
    }

    db.collection('contacts').drop(function(err, result) {
      db.createCollection('contacts', function(err, collection) {
        if (err) {
          res.status(500).send('Can\'t reset DB, try later');
          return;
        }

        collection.insert(require('../contacts'), function(err) {
          if (err) {
            res.status(500).send('Can\'t create DB, try later');
            return;
          }

          res.send('DB successfully restored');
          db.close();
        }); 
      });
    });    
  });
});

module.exports = router;
