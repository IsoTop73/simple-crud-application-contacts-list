$(function() {
  var $modal = $('.modal');
  var $buttonAdd = $('.action-add');
  var $contactsList = $('#contacts-list');
  var $buttonCancle = $('.action-cancle');
  var $buttonDelete = $('.action-delete');
  var $buttonApply = $('.action-apply');
  var $controlls = $('.controlls');
  var $miAddContact = $('#menu-item__add-contact');
  var $miEditContact = $('#menu-item__edit-contact');
  var $miDeleteContact = $('#menu-item__delete-contact');
  var $miResetDb = $('#menu-item__reset-db');
  var $checkboxes;
  var $contacts;
  var snackbar = document.querySelector('#snackbar');

  var CONTACTS;
  var deleteCount = 0;
  var localId = {};

  var contactsList = new ContactsList();

  function contactsListItem(i) {
    var photo = CONTACTS[i].photo;
    var name = CONTACTS[i].name;
    var phone = CONTACTS[i].phone;

    var contactTemplate = '<li class="mdl-list__item contact-list__item"' +
    ' data-user-id="' + i + '">' +
    '<span class="mdl-list__item-primary-content">' + (photo
      ? '<img class="contact-avatar" src="' + photo + '">'
      : '<i class="material-icons mdl-list__item-avatar">person</i>') +
    '<span class="contact-info">' +
    '<span class="contact-name">' + name + '</span>' + 
    '<span class="contact-phone">' + phone + '</span>' +
    '</span>' +
    '</span>' +
    '<span class="mdl-list__item-secondary-action">' +
    '<span class="edit-contact-icon"><i class="material-icons">edit</i></span>' +
    '<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect"' +
    'for="list-checkbox-' + i + '">' +
    '<input class="mdl-checkbox__input" type="checkbox" ' +
    'id="list-checkbox-' + i + '">' +
    '</label>' +
    '</span>' +
    '</li>';
    
    return contactTemplate;
  }

  function init() {
    $checkboxes = $contactsList.find('.mdl-checkbox');
    $contacts = $contactsList.find('.contact-list__item');
  }

  function reload() {
    setTimeout(function() {
      location.reload();
    }, 1500);
  }

  function renderContacts() {
    for (var i = 0; i < CONTACTS.length; i++) {
      $contactsList.append(contactsListItem(i));
      localId[i] = CONTACTS[i]._id;
    }

    componentHandler.upgradeDom();
  }

  function getContacts() {
    $.ajax('/contacts', {
      success: function(data) {
        CONTACTS = data;
        renderContacts();
        init();
        contactsList.bindEvents();
      },
      error: function(err) {
        snackbar.MaterialSnackbar.showSnackbar({
          message: err.responseText
        });
      }
    });
  }

  getContacts();

  function ContactsList() {
    var self = this;
    this.state = 'default';

    this.actionCancle = function() {
      if (!$modal.hasClass('hidden') && self.state !== 'addContact') {
        hideModal();
      } else {
        self.setState('default');
      }
    };

    this.actionDelete = function() {
      var deleteIds = [];

      if ($modal.hasClass('hidden')) {
        var title = 'Do you want to delete ' + deleteCount + ' contact';
        title += (deleteCount < 2) ? '?' : 's?';

        showModal(title);
      } else {
        $contacts.has('.mdl-checkbox__input:checked').each(function(i, elem) {
          var id = elem.dataset.userId;
          deleteIds.push(localId[id]);
        });

        $.ajax('/delete-contacts', {
          method: 'DELETE',
          traditional: true,
          data: {
            deleteIds: deleteIds
          },
          success: function(message) {
            snackbar.MaterialSnackbar.showSnackbar({
              message: message
            });

            reload();
          },
          error: function(err) {
            snackbar.MaterialSnackbar.showSnackbar({
              message: err.responseText
            });
          }
        });

        self.setState('default');
      }
    };

    this.actionEditContact = function() {
      if (self.state !== 'editContacts') return;
      var name = $(this).find('.contact-name').text();
      var phone = $(this).find('.contact-phone').text();

      var id = $(this).attr('data-user-id');
      var newName = name;
      var newPhone = phone;

      showModal({
        title: 'Edit: ' + name,
        inputNameValue: name,
        inputPhoneValue: phone
      });
      showControlls();

      $('#edit-contact-name-input').on('keyup', function() {
        newName = $(this).val();
      });

      $('#edit-contact-phone-input').on('keyup', function() {
        newPhone = $(this).val();
      });

      $buttonApply.on('click', function() {
        if (!newName) {
          snackbar.MaterialSnackbar.showSnackbar({
            message: 'Name is required!'
          });

          return;
        }

        if (newName === name && newPhone === phone) {
          snackbar.MaterialSnackbar.showSnackbar({
            message: 'Nothing to update!'
          });

          return;
        }

        $.ajax('/update-contact', {
          method: 'POST',
          data: {
            id: localId[id],
            newName: newName,
            newPhone: newPhone
          },
          success: function(message) {
            snackbar.MaterialSnackbar.showSnackbar({
              message: message
            });

            hideModal();
            reload();
          },
          error: function(err) {
            snackbar.MaterialSnackbar.showSnackbar({
              message: err.responseText
            });

            self.setState('default');
          }
        });
      });
    };

    this.actionAddContact = function() {
      var name = $modal.find('#add-contact-name-input').val();
      var phone = $modal.find('#add-contact-phone-input').val();

      if (!name) {
        snackbar.MaterialSnackbar.showSnackbar({
          message: 'Name is required!'
        });

        return;
      }

      $.ajax('/add-contact', {
        method: 'POST',
        data: {
          name: name,
          phone: phone
        },
        success: function(message) {
          snackbar.MaterialSnackbar.showSnackbar({
            message: message
          });

          reload();
        },
        error: function(err) {
          snackbar.MaterialSnackbar.showSnackbar({
            message: err.responseText
          });
        }
      });

      self.setState('default');
    };

    this.setState = function(state) {
      if (state === 'default') {
        reset();
        self.state = 'default';
        return;
      }

      if (state === 'deleteContacts') {
        $contactsList.removeClass('edit-contact').addClass('editing delete-contact');
      }

      if (state === 'editContacts') {
        $contactsList.removeClass('delete-contact').addClass('editing edit-contact');
      }

      if (state === 'addContact') {
        reset();
        showModal('Add contact');
      }

      self.state = state;
      showControlls();
    }

    self.bindEvents = function() {

      $buttonAdd.on('click', self.actionAddContact);
      $buttonCancle.on('click', self.actionCancle);
      $buttonDelete.on('click', self.actionDelete);
      $contacts.on('click', self.actionEditContact);

      $checkboxes.on('click', function() {
        deleteCount = $contactsList.find('.mdl-checkbox__input:checked').length;
        deleteCount ? $buttonDelete.attr('disabled', false)
        : $buttonDelete.attr('disabled', true);
      });

      $miDeleteContact.on('click', function() {
        self.setState('deleteContacts');
      });

      $miEditContact.on('click', function() {
        self.setState('editContacts');
      });

      $miAddContact.on('click', function() {
        self.setState('addContact');
      });
    }

    $miResetDb.on('click', function() {
      $.ajax('/reset-db', {
        success: function(message) {
          snackbar.MaterialSnackbar.showSnackbar({
            message: message
          });

          reload();
        },
        error: function(message) {
          snackbar.MaterialSnackbar.showSnackbar({
            message: message.responseText
          });
        }
      });
    });

    function reset() {
      $contactsList.removeClass('editing delete-contact edit-contact');

      if (self.state === 'deleteContacts') {
        for (var i = $checkboxes.length - 1; i >= 0; i--) {
          $checkboxes.eq(i).find('.mdl-checkbox__input')[0].checked = false;
        }
        $checkboxes.removeClass('is-checked');
        $buttonDelete.attr('disabled', true);
        deleteCount = 0;
      }

      hideControlls();
      hideModal();
    }

    function showControlls() {

      if (self.state === 'deleteContacts') {
        $('.show-add').addClass('hidden');
        $('.show-edit').addClass('hidden');
        $('.show-delete').removeClass('hidden');
      }

      if (self.state === 'editContacts') {
        $('.show-add').addClass('hidden');
        $('.show-delete').addClass('hidden');
        $('.show-edit').removeClass('hidden');
      }

      if (self.state === 'addContact') {
        $modal.find('.show-edit').addClass('hidden');
        $modal.find('.show-delete').addClass('hidden');
        $modal.find('.show-add').removeClass('hidden');

        return;
      }

      $controlls.removeClass('hidden');
    }

    function hideControlls() {
      $controlls.addClass('hidden');
    }

    function showModal(options) {
      $modal.removeClass('hidden').find('.mdl-card__title-text')
      .text(typeof options === 'string' ? options : options.title);

      if (self.state === 'editContacts') {
        $modal.find('#edit-contact-name-input').val(options.inputNameValue);
        $modal.find('#edit-contact-phone-input').val(options.inputPhoneValue);
        $modal.find('.show-edit .mdl-textfield').addClass('is-dirty');
      }
    }

    function hideModal() {
      $modal.addClass('hidden').find('.mdl-card__title-text').text('');
      $modal.find('.mdl-textfield').removeClass('is-dirty');
      $modal.find('input').val('');
    }
  }
});